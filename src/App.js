import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import firebase from "./firebase";

import {
  getAuth,
  signInWithPhoneNumber,
  RecaptchaVerifier,
} from "firebase/auth";

function App() {
  const [mobile, setMobile] = useState("");
  const [otp, setOTP] = useState("");

  const configrereCaptcha = () => {
    const auth = getAuth();
    window.recaptchaVerifier = new RecaptchaVerifier(
      "sign-in-button",
      {
        size: "invisible",
        callback: (response) => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          onSignInSubmit();
          alert("recaptcha Verified");
        },
        defaultCountry: "IN",
      },
      auth
    );
  };

  const onSignInSubmit = (e) => {
    e.preventDefault();
    configrereCaptcha();
    const phoneNumber = "+91" + mobile;
    debugger;

    const appVerifier = window.recaptchaVerifier;
    debugger;
    const auth = getAuth();
    debugger;
    signInWithPhoneNumber(auth, phoneNumber, appVerifier)
      .then((confirmationResult) => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        window.confirmationResult = confirmationResult;
        alert("OTP has been sent");
      })
      .catch((error) => {
        // Error; SMS not sent
        console.log(error);
        debugger;
        // ...
        alert("SMS not sent");
      });
  };

  const onSubmitOTP = (e) => {
    e.preventDefault();
    const code = otp;
    debugger;
    window.confirmationResult
      .confirm(code)
      .then((result) => {
        // User signed in successfully.
        const user = result.user;
        // ...
        debugger;
        console.log(JSON.stringify(user));
        alert("User is Verified");
        
      })
      .catch((error) => {
        // User couldn't sign in (bad verification code?)
        // ...
        alert("bad verification code");
      });
  };

  return (
    <div className="App">
      <h2>Login form</h2>

      <form onSubmit={onSignInSubmit}>
        <div id="sign-in-button"></div>
        <input
          type="number"
          name="mobile"
          placeholder="Mobile Number"
          value={mobile}
          required
          onChange={(e) => setMobile(e.target.value)}
        />
        <button type="submit">Submit</button>
      </form>
      <h2>Enter OTP</h2>
      <form onSubmit={onSubmitOTP}>
        <input
          type="number"
          name="otp"
          placeholder="OTP Number"
          value={otp}
          onChange={(e) => setOTP(e.target.value)}
          required
        />
        <button type="submit">Verify</button>
      </form>
    </div>
  );
}

export default App;
